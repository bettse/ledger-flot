<?php Header("content-type: application/x-javascript"); ?>
year_forecast = new Array();
<?php

include "config.php";
$year_forecast = ' -w -J --forecast "d<=[today]+365" -d "d>=[next month] & d<=[today]+365" --sort d reg "FirstTech:Checking"'; 
unset($output);
exec("$ledger $year_forecast", $output);

foreach ($output as $line){
    $tmp = explode(" ", $line);
    print "year_forecast.push([new Date(\"$tmp[0]\").getTime(), $tmp[1]]);\n"; 
}
?>

$(document).ready(function() {

    $.plot($("#year_forecast"), [ {data:year_forecast, label:"Forecast of FirstTech checking balance for the next 365 days"} ], {
           xaxis: {
               mode: "time"
           },
           series: {
               lines: { show: true },
               points: { show: true }
           },
            grid: { hoverable: true, clickable: true },
        });


});
