<?php Header("content-type: application/x-javascript"); ?>
bva = new Array();
<?php

include "config.php";

$budget = ' -w -F "%(account)\t%(amount)\n" -M -p "next month" --forecast "d<=[next month]" reg ^exp | sed -e \'s/\$//g\' | sed -e \'s/,//g\' ';

unset($output);
exec("$ledger $budget", $output);

foreach ($output as $line){
    //make into key-value pairs
    $tmp = explode("\t", $line);
    if($tmp[0] != "Expenses:Discretionary"){
        $categorylist[] = $tmp[0];
        $budgetlist[$tmp[0]] = $tmp[1] * 1;
    }
}

foreach($categorylist as $key){

    $tmp = substr(strrchr($key, ":"), 1);
    $pos = strpos($tmp, " ");
    if($pos > 0){
        $parameter= substr($tmp, 0, $pos) . " ";
    }else{
        $parameter= $tmp . " ";
    }
    
    $averages = ' -w -F "%(date)\t%(amount)\n" -E -MA -c reg ' . $key . ' | sed -e \'s/\$//g\' | sed -e \'s/,//g\'';
    
    unset($output);
    exec("$ledger $averages", $output);
    print "tmp = new Array()\n";
    foreach ($output as $line){
        //make into key-value pairs
        $tmp = explode("\t", $line);
        $diff = ($tmp[1]*1) - ($budgetlist[$key]*1); 
        print "tmp.push([new Date(\"$tmp[0]\").getTime(), $diff]);\n"; 
    }
    print "bva.push({label:\"$key\", data: tmp});\n"; 
}

?>

$(document).ready(function() {
    $.plot($("#budgetvsaverage"), bva, {
           xaxis: {
               mode: "time"
           },
           series: {
               lines: { show: true },
               points: { show: true }
           },
            grid: { hoverable: true, clickable: true },
           legend: {show: true, position:"nw" },
        });

});
