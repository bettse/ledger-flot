<?php Header("content-type: application/x-javascript"); ?>
daily_balance = new Array();
<?php

include "config.php";
$daily_balance = ' -w -J -c -p "daily" -d "d>=[this month] & d < [next month]"  --sort d reg "FirstTech:Checking" ';
unset($output);
exec("$ledger $daily_balance", $output);

foreach ($output as $line){
    //make into key-value pairs
    $tmp = explode(" ", $line);
    print "daily_balance.push([new Date(\"$tmp[0]\").getTime(), $tmp[1]]);\n"; 
}
?>

$(document).ready(function() {
    $.plot($("#daily_balance"), [ {data:daily_balance, label:"Combined checking account balance"} ], {
           xaxis: {
               mode: "time"
           },
           series: {
               lines: { show: true },
               points: { show: true }
           },
            grid: { hoverable: true, clickable: true },
        });

});
