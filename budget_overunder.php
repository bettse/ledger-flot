<?php Header("content-type: application/x-javascript"); ?>
overunder = new Array();
overunderlabels = new Array();
<?php
include "config.php";

$budget_overunder = ' -c -w -F "%(account)\t%(amount)\n" -E -p "this month" --budget -M reg ^exp | sed -e \'s/\$//g\' | sed -e \'s/,//g\' ';
unset($output);
exec("$ledger $budget_overunder", $output);
$i = 0;
foreach ($output as $line){
    //make into key-value pairs
    $tmp = explode("\t", $line);
    $labellist[] = $tmp[0];
    print "overunder.push([$i, $tmp[1]]);\n"; 
    print "overunderlabels.push([$i, \"$tmp[0]\"]);\n"; 
    $i++;
}

?>

$(document).ready(function() {
    $.plot($("#budget_overunder"), [ {data: overunder, bars: { show: true  }} ], {
            grid: { hoverable: true, clickable: true },
            xaxis: {
               ticks: overunderlabels
            },

    });
});
