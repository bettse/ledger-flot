<?php Header("content-type: application/x-javascript"); ?>
piepieces = new Array();
<?php

include "config.php";
$breakdown = ' -c -w -F "%(account)\t%(total)\n" -p "this month" bal ^exp | sed -e \'s/\$//g\' | sed -e \'s/,//g\' ';
unset($output);
exec("$ledger $breakdown", $output);
$i=0;
foreach ($output as $line){
    //make into key-value pairs
    $tmp = explode("\t", $line);
    if($tmp[0] != "Expenses" && $tmp[0] != "" && $tmp[0] != "Expenses:Bills" && $tmp[0] != "Expenses:Food" && $tmp[0] != "Expenses:Discretionary"){
        print "piepieces.push({label:\"$tmp[0]\", data: $tmp[1]});\n"; 
    }
    $i++;
}


?>
$(document).ready(function() {
    $.plot($("#month_breakdown"), piepieces, 
    {
        series: {
            pie: { 
                show: true
            }
        }
    });
        
    var previousPoint = null
    $("#month_breakdown").bind("plothover", function (event, pos, item) {
        $("#x").text(pos.x.toFixed(2));
        $("#y").text(pos.y.toFixed(2));

        if (item) {
            if (previousPoint != item.datapoint) {
                previousPoint = item.datapoint;
                
                $("#tooltip").remove();
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                
                showTooltip(item.pageX, item.pageY,
                            "$" + y);
            }
        }
        else {
            $("#tooltip").remove();
            previousPoint = null;            
        }
    });

});
