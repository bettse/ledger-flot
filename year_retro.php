<?php Header("content-type: application/x-javascript"); ?>
ytd = new Array();
<?php

include "config.php";

$ytd = '-w -J -d "d<=[today] & d>=[today]-365" --weekly --sort d reg "FirstTech:Checking"'; 
unset($output);
exec("$ledger $ytd", $output);

foreach ($output as $line){
    $tmp = explode(" ", $line);
    print "ytd.push([new Date(\"$tmp[0]\").getTime(), $tmp[1]]);\n"; 
}
?>

$(document).ready(function() {

    $.plot($("#ytd"), [ {data:ytd, label:"FirstTech checking balance of the last year to date"} ], {
           xaxis: {
               mode: "time"
           },
           series: {
               lines: { show: true },
               points: { show: true }
           },
            grid: { hoverable: true, clickable: true },
        });


});
