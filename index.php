<html>
<head>
<title>Ledger - Flot</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<!-- iPad Setup -->
<meta name="viewport" content="minimum-scale=1.0, maximum-scale=1.0,
width=device-width, user-scalable=no">
<script language="javascript" type="text/javascript" src="js/jquery.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.flot.pie.js"></script>
<script language="javascript" type="text/javascript" src="js/ledger-flot.js"></script>
<script type="text/javascript" src="js/jquery.cycle.all.min.js"></script>
<script type="text/javascript" src="js/jquery.touchwipe.min.js"></script>

<script language="javascript" type="text/javascript" src="budget_overunder.php"></script>
<script language="javascript" type="text/javascript" src="year_forecast.php"></script>
<script language="javascript" type="text/javascript" src="year_retro.php"></script>
<script language="javascript" type="text/javascript" src="daily_balance.php"></script>
<script language="javascript" type="text/javascript" src="month_breakdown.php"></script>
<link rel="stylesheet" type="text/css" href="css/ledger-flot.css" /> 

</head>
<body>

<div id="imagegallery" onclick="javascript:$('#imagegallery').cycle('toggle');">

<div id="budget_overunder" class="ledger-chart"></div> 

<div id="daily_balance" class="ledger-chart"></div>

<div id="year_forecast" class="ledger-chart"></div>

<div id="month_breakdown" class="ledger-chart"></div>
<div id="ytd" class="ledger-chart"></div>
</div>

</body>
</html>
